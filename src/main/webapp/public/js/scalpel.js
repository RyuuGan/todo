(function($) {

// ========
// Standard jQuery extensions
// ========

// Find inside element or match itself

  $.fn.lookup = function(selector) {
    var elems = this.find(selector);
    if (this.is(selector))
      return elems.add(this);
    else return elems;
  };

// Insert at index function

  $.fn.insertAt = function(index, element) {
    var lastIndex = this.children().size();
    if (index < 0) {
      index = Math.max(0, lastIndex + 1 + index)
    }
    this.append(element);
    if (index < lastIndex) {
      this.children().eq(index).before(this.children().last());
    }
    return this;
  };

// Insert-at-caret

  $.fn.insertAtCaret = function(value) {
    return this.each(function() {
      if (document.selection) {
        this.focus();
        var sel = document.selection.createRange();
        sel.text = value;
        this.focus();
      } else if (typeof(this.selectionStart) != "undefined") {
        var startPos = this.selectionStart;
        var endPos = this.selectionEnd;
        var scrollTop = this.scrollTop;
        this.value = this.value.substring(0, startPos) +
            value + this.value.substring(endPos,this.value.length);
        this.focus();
        this.selectionStart = startPos + value.length;
        this.selectionEnd = startPos + value.length;
        this.scrollTop = scrollTop;
      } else {
        this.value += value;
        this.focus();
      }
    })
  };

// Scroll to element

  $.fn.scrollTo = function() {

    var scrollTop = -1;

    this.each(function() {
      scrollTop = $(this).offset().top;
    });

    if (scrollTop >= 0)
      $("html, body").animate({
        "scrollTop": scrollTop
      }, 200);
  };



// ========
// ScalpelJS base
// ========

  $.scalpel = {
    queue: {},
    init: function(ctx) {
      $.each($.scalpel.queue, function(selector, handler) {
        ctx.lookup(selector).each(handler);
      });
    },
    settings: {},
    log: function(text) {
      if (typeof(console) != "undefined" && typeof(console.log) == "function")
        console.log(text);
    }
  };

  $(function() {
    $.scalpel.init($("body"));
  });


// Localization

  $.msg = {};


// Notices

  $.scalpel.notices = (function() {

    var s = $.scalpel.settings;

    // Close on escape
    window.addEventListener("keydown", function(ev) {
      if (ev.keyCode == 0x1B) {
        clear();
      }
    });

    function timeout() {
      var t = s['notices.timeout'];
      return t ? parseInt(t) : 10000;
    }

    function container() {
      return $("#notices");
    }

    function hasErrors() {
      return $(".notice.error:not(:animated)", container()).size() > 0;
    }

    function stash() {
      if (!window.sessionStorage) return;
      var cnt = container();
      // Delete animating notices before stashing
      $(".notice:animated", cnt).remove();
      window.sessionStorage.setItem("scalpel.notices", cnt.html());
      cnt.empty();
    }

    function unstash() {
      if (!window.sessionStorage) return;
      var html = window.sessionStorage.getItem("scalpel.notices");
      if (html) {
        container().append(html);
        window.sessionStorage.removeItem("scalpel.notices");
        $(".notice", container()).each(function() {
          initElem(this);
        });
      }
    }

    function initElem(elem) {
      var e = $(elem);
      $(".hide", e).remove();
      var handle = $("<span class='hide'/>");
      handle.bind("click.scalpel.notices", function() {
        dispose(e);
      });
      e.prepend(handle);
      setTimeout(function() {
        dispose(e);
      }, timeout());
    }

    function mkElem(notice) {
      var kind = notice.kind;
      var msg = notice.msg
          .replace(/&quot;/g, "\"")
          .replace(/&lt;/g,"<")
          .replace(/&gt;/g,">")
          .replace(/&amp;/g, "&");
      var e = $("<div class=\"notice " + kind + "\">" + msg +"</div>");
      initElem(e);
      return e;
    }

    function add(notice) {
      container().append(mkElem(notice));
    }

    function addError(msg) {
      add({kind: "error", msg: msg});
    }

    function addWarn(msg) {
      add({kind: "warn", msg: msg});
    }

    function addInfo(msg) {
      add({kind: "info", msg: msg});
    }

    function addAll(data) {
      if (data && data.notices) {
        for (var i in data.notices) {
          var n = data.notices[i];
          add(n);
        }
      }
    }

    function clear() {
      dispose($(".notice", container()));
    }

    function dispose(elems) {
      elems.fadeOut("fast", function() {
        $(this).remove();
      });
    }

    $.scalpel.queue["#notices"] = unstash;

    return {
      timeout: timeout,
      container: container,
      hasErrors: hasErrors,
      stash: stash,
      unstash: unstash,
      initElem: initElem,
      mkElem: mkElem,
      clear: clear,
      dispose: dispose,
      add: add,
      addInfo: addInfo,
      addWarn: addWarn,
      addError: addError,
      addAll: addAll
    };

  })();


// AJAX helpers

  $.scalpel.ajax = (function() {

    var notices = $.scalpel.notices;
    var viewport = $.scalpel.viewport;

    $.msg["ajax.authRequired"] = "You need to login to continue.";
    $.msg["ajax.serverDown"] = "Server is down for scheduled maintenance.";
    $.msg["ajax.requestTimeout"] = "Request took too long to process. Please try again soon.";
    $.msg["ajax.accessDenied"] = "You have insufficient permissions to access the resource.";
    $.msg["ajax.failed"] = "Your request could not be processed. Please contact technical support.";

    var inputTypes = ["text", "checkbox", "radio", "select", "textarea", "date"];

    function processResponse(data, stay) {
      notices.clear();
      notices.addAll(data);
      if (data.redirect && !stay) {
        $.scalpel.viewport.navigate(data.redirect, true);
      }
    }

    function processErrors(xhr) {
      if (xhr.status == 0) return true;
      else if (xhr.status == 401)
        $.scalpel.notices.addWarn($.msg["ajax.authRequired"]);
      else if (xhr.status == 502)
        $.scalpel.notices.addError($.msg["ajax.serverDown"]);
      else if (xhr.status == 504)
        $.scalpel.notices.addError($.msg["ajax.requestTimeout"]);
      else if (xhr.status == 403)
        $.scalpel.notices.addError($.msg["ajax.accessDenied"]);
      else $.scalpel.notices.addError($.msg['ajax.failed']);
      return false;
    }

    return {
      processResponse: processResponse,
      processErrors: processErrors,
      inputTypes: inputTypes
    };

  })();

// Partial viewports

  $.scalpel.viewport = (function() {

    var s = $.scalpel.settings;

    $.msg['standby'] = "Please wait...";

    // Binding global onpopstate event on load
    if (typeof(history.replaceState) == "function") {
      $(function() {
        window.addEventListener("popstate", function(ev) {
          if (ev.state && ev.state.href)
            load(ev.state.href);
        }, false);
      });
    }

    function timeout() {
      var t = s['viewport.timeout'];
      return t ? parseInt(t) : 500;
    }

    function container() {
      var selector = s['viewport.container'];
      return selector ? $(selector) : $("#viewport");
    }

    function overlay() {
      var overlay = $('<div id="viewport-standby"></div>');
      overlay.hide();
      overlay.append('<div class="message">' + $.msg['standby'] + '</div>');
      return overlay;
    }

    // Loads HTML content from specified location and
    // inserts it into the container (`#viewport` by default).
    function load(href) {
      // Trigger viewport unload
      var e = $.Event("viewportUnload");
      $(window).trigger(e);
      if (e.isDefaultPrevented()) return;
      // Adding "Please wait" overlay, hidden initially
      var o = overlay();
      $("body").append(o);
      // When timeout comes, show the overlay
      var i = setTimeout(function() {
        o.fadeIn(300);
      }, timeout());
      // Perform AJAX request
      $.ajax({
        url: href,
        headers: {"X-Render-Mode": "partial-viewport" },
        dataType: "html",
        data: {
          "__": new Date().getTime().toString()
        },
        type: "GET",
        success: function(data) {
          var cnt = $(data);
          // Remove the overlay
          o.remove();
          clearTimeout(i);
          // Insert the data
          container().replaceWith(cnt).remove();
          // Initialize the container
          $.scalpel.init(cnt);
          // See if site title needs to be replaced
          var newTitle = cnt.attr("data-title");
          if (newTitle) {
            $("head title").text(newTitle);
          }
          // Raise the event
          $("body").trigger("viewportLoad");
          // Navigate to #anchor
          scrollToAnchor();
        },
        error: function(xhr) {
          $.scalpel.ajax.processErrors(xhr);
          // Remove the overlay
          o.remove();
          clearTimeout(i);
        }
      });
    }

    // Navigates to specified `url` using viewport loading
    // if the URL starts with `data-viewport-prefix` attribute
    // of `body`, otherwise navigates by standard means
    function navigate(url, redirect) {
      // determine if replaceState can be used
      var useHistory = true;
      if (!window.history.replaceState)
        useHistory = false;
      var prefix = $("body").attr("data-viewport-prefix");
      if (!prefix || url.indexOf(prefix) != 0)
        useHistory = false;
      if (useHistory) {
        if (redirect)
          window.history.replaceState({href: url}, "", url);
        else
          window.history.pushState({href: url}, "", url);
        load(url);
      } else {
        $.scalpel.notices.stash();
        if (redirect) window.location.replace(url);
        else window.location.href = url;
      }
    }

    function scrollToAnchor() {
      var hash = location.href.replace(/.*?(?=#|$)/, "");
      var scrollTarget = $(hash);
      if (scrollTarget.size() == 0)
        scrollTarget = $(".viewport-anchor").first();
      scrollTarget.scrollTo();
    }

    // If `data-viewport-prefix` attribute is set on body,
    // initialize every link which starts with that prefix
    // with partial viewport functionality.
    var prefix = $("body").attr("data-viewport-prefix");
    if (prefix) {
      $.scalpel.queue["a[href^='" + prefix + "']"] = function() {
        var a = $(this);
        if (a.attr("rel") == "popup" ||
            a.attr("target") == "_blank")
          return;
        a.addClass("partial-link");
        a.unbind(".scalpel.partial-link")
            .bind("click.scalpel.partial-link", function(ev) {
              if (ev.button == 0 && !(ev.metaKey || ev.ctrlKey)) {
                ev.preventDefault();
                var href = $(this).attr("href");
                navigate(href);
                return false;
              } else return true;
            });
      }
    }

    return {
      timeout: timeout,
      container: container,
      load: load,
      navigate: navigate,
      scrollToAnchor: scrollToAnchor
    };

  })();

  $.scalpel.placeholder = function() {
    return $('<div class="loading"/>')
  };

  $.scalpel.queue['[data-ref]'] = function() {
    var selector = $(this).attr("data-ref");
    var cssClass = $(this).attr("data-ref-class");
    if (!cssClass)
      cssClass = "active";
    $(selector).addClass(cssClass);
  };

  $.scalpel.queue['.focus'] = function() {
    $(this).focus();
  };

  $.scalpel.queue['[data-set-focus]'] = function() {
    $(this).unbind(".scalpel.setFocus")
        .bind("click.scalpel.setFocus", function () {
          $($(this).attr("data-set-focus")).focus();
        });
  };

  $.scalpel.queue['[data-show]'] = function() {
    $(this).unbind(".scalpel.dataShow")
        .bind("click.scalpel.dataShow", function () {
          $($(this).attr("data-show")).show();
        });
  };

  $.scalpel.queue['[data-hide]'] = function() {
    $(this).unbind(".scalpel.dataHide")
        .bind("click.scalpel.dataHide", function () {
          $($(this).attr("data-hide")).hide();
        });
  };

  $.scalpel.ibox = (function() {

    $.msg["Close"] = "Close";

    function get() {
      init();
      return $("#ibox");
    }

    function init() {
      var cnt = $("#ibox");
      if (cnt.size() > 0)
        return;
      cnt = $('<div id="ibox"></div>');
      cnt.hide();
      $("body").prepend(cnt);
      $(window)
          .unbind(".ibox")
          .bind("keydown.ibox", function(ev) {
            if (ev.keyCode == 0x1B)
              close();
          });
      cnt.bind("click.ibox", function(ev) {
        close();
      });
    }

    function close() {
      var cnt = $("#ibox");
      $("iframe", cnt).remove();
      cnt.fadeOut(300, function() {
        $(this).remove();
      });
    }

    function isVisible() {
      return $("#ibox").is(":visible");
    }

    function load(href) {
      $.ajax({
        url: href,
        dataType: "html",
        type: "GET",
        data: {
          "__": new Date().getTime().toString()
        },
        success: function(data) {
          show(data);
        },
        error: $.scalpel.ajax.processErrors
      });
    }

    function show(data) {
      // Trigger viewport unload
      var e = $.Event("viewportUnload");
      $(window).trigger(e);
      if (e.isDefaultPrevented()) return;
      // Show ibox
      var cnt = get().empty();
      var wrapper = $('<div id="ibox-wrapper"></div>');
      wrapper.append(data);
      cnt.append(wrapper);
      var _close = $('<div id="ibox-close"></div>');
      _close.attr("title", $.msg['Close']);
      wrapper.append(_close);
      cnt.fadeIn(300, function() {
        $("body").animate({scrollTop: cnt.offset().top}, 300);
        cnt.trigger("ibox_load");
        $.scalpel.init(wrapper);
        wrapper
            .unbind(".ibox")
            .bind("click.ibox", function(ev) {
              ev.stopPropagation();
            });
        _close.bind("click.ibox", function() {
          close();
        });
      });
    }

    return {
      get: get,
      init: init,
      close: close,
      isVisible: isVisible,
      show: show,
      load: load
    };

  })();

  $.scalpel.queue['form.partial'] = function() {

    var form = $(this);

    var stay = form.hasClass("stay");
    var readonly = form.hasClass("readonly");
    var action = form.attr("action");
    var method = form.attr("method").toUpperCase();

    form.unbind(".scalpel.partial")
        .bind("submit.scalpel.partial", function(ev, data) {
          if (readonly) return false;
          // Replace submission buttons with loading placeholder temporarily
          var ph = $.scalpel.placeholder();
          var submits = $(".submits", form);
          var h = submits.height();
          submits.hide();
          ph.insertAfter(submits);
          ph.height(h);
          // Prepare params
          var params = $(":not(.exclude)", form).serializeArray();
          params.push({
            name: "__",
            value: new Date().getTime().toString()
          });
          if (data && data.params) {
            params = params.concat(data.params);
          }
          // Execute pre-submit event
          if (typeof(form[0]._preSubmit) == "function") {
            form[0]._preSubmit({ params: params });
          }
          $.scalpel.log(method + " " + action + " " + JSON.stringify(params));
          if (method == "GET") {
            var url = action + "?" + $.param(params);
            $.scalpel.viewport.navigate(url);
          } else {
            // Perform ajax submit
            $.ajax({
              data: params,
              dataType: "json",
              url: action,
              type: method,
              success: function(data) {
                form.trigger("postSubmit", data);
                $.scalpel.ajax.processResponse(data, stay);
                submits.show();
                ph.remove();
                $("input[type='password'], .cleanup", form).val("");
              },
              error: function(xhr) {
                submits.show();
                ph.remove();
                $.scalpel.ajax.processErrors(xhr);
              }
            });
          }
          // Prevent actual submit
          ev.preventDefault();
          return false;
        });

  };

  $.scalpel.switcher = (function() {

    // Hide all open targets on click outside
    $(function() {
      $("html").bind("click.scalpel.switch", function() {
        hideAll()
      });
    });

    function hideAll() {
      $(".switch-target").hide();
      $("[data-switch]").each(function() {
        var e = $(this);
        var cssClass = e.attr("data-switch-class");
        if (!cssClass)
          cssClass = "active";
        e.removeClass(cssClass);
      });
    }

    return {
      hideAll: hideAll
    }

  })();

  $.scalpel.queue['[data-switch]'] = function() {

    var switcher = $(this);

    var cssClass = switcher.attr("data-switch-class");
    if (!cssClass)
      cssClass = "active";

    var target = $(switcher.attr("data-switch"));
    target.addClass("switch-target");

    if (switcher.hasClass("switch-stay"))
      target.click(function(ev) {
        ev.stopPropagation();
      });

    function update() {
      if (target.is(":visible"))
        switcher.addClass(cssClass);
      else switcher.removeClass(cssClass);
    }

    update();

    function toggle() {
      switcher.toggleClass(cssClass);
      target.toggle(0, update);
    }

    switcher.bind("click.scalpel.switch", function() {
      $.scalpel.switcher.hideAll();
      toggle();
      return false;
    });

  };

  $.scalpel.queue['.toggler'] = function() {

    var $this = $(this);

    var buttons = $("*[data-for]", $this);
    var cssClass = $this.attr("data-toggler-class");

    if (!cssClass)
      cssClass = "active";

    function updateButtons() {
      buttons.each(function() {
        var b = $(this);
        var target = $(b.attr("data-for"));
        if (target.is(":visible"))
          b.addClass(cssClass);
        else b.removeClass(cssClass);
      });
    }

    buttons.unbind(".scalpel.toggler")
        .bind("click.scalpel.toggler", function(ev) {
          ev.preventDefault();
          var b = $(this);
          // hide all targets
          buttons.each(function() {
            $($(this).attr("data-for")).hide();
          });
          // show this one
          $(b.attr("data-for")).show();
          // update buttons state
          updateButtons();
          return false;
        });

    updateButtons();
  };
})(jQuery);