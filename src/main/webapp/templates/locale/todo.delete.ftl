[#ftl]

[@me]

  ## Todo deletion

  You are going to delete todo:

  ```
    ${todo.description}
  ```

  Are you sure you want to delete this `todo`?
[/@me]