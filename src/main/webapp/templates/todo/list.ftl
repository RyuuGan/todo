[#ftl]

[#assign sorting = request.params["s"]!"priority"/]
[#assign sortingOptions = ["priority", "dueDate"]/]
[#assign completed = (request.params["c"]!"false") == "true"/]

[#if !sortingOptions?seq_contains(sorting)]
  [#assign sorting = "priority"/]
[/#if]

[#assign content]
<div class="pad">
  [#if todos?size > 0]
    <div class="todo-list">
      <div>
        <a href="/~new" class="btn partial-link">${msg['todo.new']}</a>
      </div>
      <form id="sorting-form"
            class="partial margin"
            method="get"
            action="/">
        <div class="ctl-group">
        ${msg['sorting']}
        </div>
        [#list sortingOptions as s]
          <div>
            <input id="sorting-${s}"
                   type="radio"
                   name="s"
                   value="${s}"
                   [#if s == sorting]checked="checked"[/#if]/>
            <label for="sorting-${s}">${msg['sorting.${s}']}</label>
          </div>
        [/#list]
        <div>
          <input id="c" type="checkbox" value="true" name="c" [#if completed]checked="checked"[/#if]/>
          <label for="c">${msg['todo.completed.show']}</label>
        </div>
      </form>
      [#list todos as todo]
        <div class="elem">
          <div class="description">${todo.description}</div>
          [#if todo.priority?? && !todo.isCompleted]
            <div class="priority" title="${msg['todo.priority']}">${todo.priority}</div>
          [/#if]
          [#if todo.isCompleted]
            <div class="priority" ><img src="/img/icons/32/checkmark.png"/> </div>
          [/#if]
          <div class="ctls">
            <a href="/${todo.id}" class="partial-link"><img src="/img/icons/32/pencil.png"/></a>
            <a href="/${todo.id}/~delete" class="partial-link"><img src="/img/icons/32/delete.png"/></a>
          </div>
          [#if todo.dueStartDate?? || todo.dueEndDate??]
            <div class="dates">
              [#if todo.dueStartDate??]
              <div class="due-start-date" title="${msg['todo.startDate.hint']}">${todo.dueStartDate?string("yyyy-MM-dd")}</div>
              [/#if]
              [#if todo.dueEndDate??]
              <div class="due-end-date" title="${msg['todo.endDate.hint']}">${todo.dueEndDate?string("yyyy-MM-dd")}</div>
              [/#if]
            </div>
          [/#if]
        </div>
      [/#list]
    </div>
  [#else]
    <p class="no-items">${msg['todo.empty']}</p>
  [/#if]
</div>

<script type="text/javascript">
  $.scalpel.queue["#sorting-form"] = function() {
    var form = $(this);
    var sortingInputs = $("[name=s]", form);
    sortingInputs.change(function(){
      form.submit();
    });
    $("#c").change(function(){
      form.submit();
    });
  }
</script>
[/#assign]

[#include "layout.ftl"/]