package org

import circumflex._, core._

package object todo {
  val log = new Logger("org.todo")

  val domain = cx.getString("domain").getOrElse("localhost:18010")
  val domainSecure = cx.getString("domain.secure").getOrElse("localhost:18010")
  val domainCdn = cx.getString("domain.cdn").getOrElse("localhost:18010")

  def currentUser = auth.principalOption.getOrElse(throw new IllegalStateException("User is not authenticated."))
}