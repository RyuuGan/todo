package org.todo

import circumflex._, core._, orm._, web._

trait BaseRecord[R <: BaseRecord[R]]
    extends Record[Long, R]
    with IdentityGenerator[Long, R] { this: R =>

  def PRIMARY_KEY: ValueHolder[Long, R] = id

  val id = "id".BIGINT.NOT_NULL.AUTO_INCREMENT

  def toJSONObject = {
    val fields = relation.fields.flatMap { field =>
      val f = relation.getField(this, field)
      if (!f.isEmpty)
        Some("\"" + f.name + "\": \"" + f.apply().toString + "\"")
      else None
    }.mkString(", ")

    "{ " + fields + " }"
  }

  def updatePrimitiveFieldsFromParams() {
    relation.fields.foreach { field =>
      val f = relation.getField(this, field)
      val value = request.params.get(f.name)
      if (PRIMARY_KEY != f && !f.isInstanceOf[Association[_, _, _]])
        f match {
          case x: XmlSerializable[_, _] =>
            value.map(v => x.set(x.fromString(v)))
          case _ =>
        }
    }
  }
}

trait BaseTable[R <: BaseRecord[R]]
    extends Table[Long, R] { this: R =>

  def fetch(id: String): R = fetchOption(id).getOrElse(sendError(404))

  def fetchOption(id: String): Option[R] = try {
    get(id.trim.toLong)
  } catch {
    case e: NumberFormatException => None
  }
}