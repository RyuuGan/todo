package org.todo

import circumflex._, core._, web._, freemarker._

class AuthRouter extends Router {

  get("/login") = {
    'nonce := randomString(5)
    ftl("/auth/login.ftl")
  }

  post("/login") = partial {
    User.lookup(param("e")) match {
      case Some(u) =>
        val n = param("n")
        val token = param("t")
        if (token != u.token(n))
          throw new ValidationException("User.notFound")

        // always remember user that was logged in
        auth.login(u, true)
        'redirect := auth.defaultReturnLocation
      case _ =>
        throw new ValidationException("User.notFound")
    }
  }

  get("/logout") = {
    auth.logout()
    sendRedirect(auth.defaultReturnLocation)
  }
}