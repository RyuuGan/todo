package org.todo

import circumflex._, security._

object auth extends Auth[User] {
  def defaultReturnLocation: String = "/"

  def lookup(principalId: String): Option[User] = User.fetchOption(principalId)

  def anonymous: User = AnonymousUser

  def secureDomain: String = domainSecure

  def loginUrl: String = "/auth/login"
}