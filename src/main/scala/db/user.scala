package org.todo

import circumflex._, core._, orm._, security._

class User
    extends BaseRecord[User]
    with Principal {

  def relation: Relation[Long, User] = User

  val email = "email".TEXT.NOT_NULL
  val title = "title".HTML.NOT_NULL

  val pswdSha256 = "pswdSha256".TEXT.NOT_NULL
  def setPassword(password: String) {
    pswdSha256 := sha256(password)
  }

  def uniqueId: String = id().toString
  def secret: String = pswdSha256()

  def token(nonce: String) = sha256(pswdSha256()  + ":" + nonce)
}

object User extends User with BaseTable[User] {

  private val u = User AS "u"

  val emailUniqueKey = UNIQUE(email)

  validation
      .notEmpty(_.title)
      .notEmpty(_.email)

  def lookup(email: String) =
      SELECT(u.*)
          .FROM(u)
          .add(u.email EQ email)
          .unique()
}

object AnonymousUser extends User {
  id.set(-1)
  title.set("Anonymous")
  pswdSha256.set("")
}