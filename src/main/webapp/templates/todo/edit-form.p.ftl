[#ftl]

[#assign todo = todo!{}/]

<div class="ctl-group">
  <label for="d">${msg['todo.description']}</label>
  <textarea rows="8" id="d" name="d" class="field focus">${todo.description!}</textarea>
</div>
<div class="ctl-group">
  <label for="p">${msg['todo.priority']}</label>
  <input type="text" class="field" id="p" name="p" value="${todo.priority!}">
</div>
<div class="ctl-group">
  <label for="dsd">${msg['todo.startDate']}</label>
  <input type="text" class="field" id="dsd" name="dsd" value="${(todo.dueStartDate?string("yyyy-MM-dd"))!}">
</div>
<div class="ctl-group">
  <label for="ded">${msg['todo.endDate']}</label>
  <input type="text" class="field" id="ded" name="ded" value="${(todo.dueEndDate?string("yyyy-MM-dd"))!}">
</div>
<div>
  <input type="checkbox" value="true" name="c" id="c" [#if (todo.isCompleted)!false]checked="checked"[/#if]>
  <label for="c">${msg['todo.completed']}</label>
</div>