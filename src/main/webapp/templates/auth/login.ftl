[#ftl]

[#assign main]
<form id="login-form"
      action="/auth/login"
      method="post"
      class="partial pad">
  <h2>${msg['login']}</h2>
  <div class="ctl-group">
    <label for="e">${msg['user.email']}</label>
    <input type="text"
           id="e"
           class="field focus"
           name="e"/>
  </div>
  <div class="ctl-group">
    <label for="w">${msg['user.password']}</label>
    <input type="password"
           class="field"
           id="w"/>
  </div>
  <div class="submits centered">
    <input type="submit" value="${msg['login']}" class="btn"/>
  </div>
</form>
<script type="text/javascript">
  $.scalpel.queue['#login-form'] = function(){
    $(this)[0]._preSubmit = function(obj) {
      var nonce = apiV1.nonce();
      obj.params.push({
        "name": "n",
        "value": nonce
      });
      var secret = $.sha256($.sha256($("#w").val()) + ":" + nonce);
      obj.params.push({
        "name": "t",
        "value": secret
      })
    }
  }
</script>
[/#assign]

[#include "layout.ftl"/]