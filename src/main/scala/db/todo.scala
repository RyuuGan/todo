package org.todo

import circumflex._, core._, orm._, web._
import java.util.Date

class Todo
    extends BaseRecord[Todo] {

  def relation: Relation[Long, Todo] = Todo

  val user = "user_id".BIGINT.NOT_NULL
      .REFERENCES(User).ON_DELETE(CASCADE).ON_UPDATE(CASCADE)

  val priority = "priority".NUMERIC(10,2)
  val creationDate = "creation_date".TIMESTAMP.NOT_NULL(new Date)

  // Due dates
  val dueStartDate = "due_start_date".DATE
  val dueEndDate = "due_end_date".DATE

  // Completion timestamp: if `todo` is completed this date is not empty
  val completionDate = "completion_date".DATE
  def isCompleted = !completionDate.isEmpty

  val description = "description".HTML.NOT_NULL

  def updateFromParams() = {
    description := param("d")
    dueStartDate.set(dueStartDate.fromString(param("dsd")))
    dueEndDate.set(dueEndDate.fromString(param("ded")))
    priority.set(priority.fromString(param("p")))
    if (param("c") == "true")
      completionDate := new Date
    else completionDate.setNull()
  }

}
object Todo extends Todo with BaseTable[Todo] {

  private val t = Todo AS "t"

  validation
      .notEmpty(_.description)

  def find(user: User, sorting: String = "priority", completed: Boolean = false) = {
    val s = SELECT(t.*)
        .FROM(t)
        .add(t.user IS user)

    sorting match {
      case "priority" => s.ORDER_BY(t.priority DESC)
      case "dueDate" => s.ORDER_BY(dueEndDate DESC)
      case _ => s.ORDER_BY(t.priority DESC)
    }
    if (!completed)
      s.add(t.completionDate IS_NULL)

    s.list()
  }

}