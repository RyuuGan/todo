package org.todo

import circumflex._, core._, web._
import scala.collection.mutable.{ListBuffer, HashMap}

object json {
  def toString[R <: BaseRecord[R]](records: Seq[R]) = {
    "[ " + records.map(_.toJSONObject).mkString(", ") + " ]"
  }
}

object apiPartial {

  protected def _paramsMap =
    ctx.getOrElseUpdate("partial.params", new HashMap[String, String])
        .asInstanceOf[HashMap[String, String]]

  def paramsMap = _paramsMap.toMap

  def addParam(key: String, value: String): this.type = {
    _paramsMap += key -> value
    this
  }

  def removeParam(key: String): this.type = {
    _paramsMap -= key
    this
  }

  protected def _paramsJSONMap =
    ctx.getOrElseUpdate("partial.params.json", new HashMap[String, String])
        .asInstanceOf[HashMap[String, String]]

  def JSONParams = _paramsJSONMap.toMap

  def addJSONParam(key: String, json: String): this.type = {
    _paramsJSONMap += key -> json
    this
  }

  def removeJSONParam(key: String): this.type = {
    _paramsJSONMap -= key
    this
  }

  def recovers = ctx.getAs[ListBuffer[() => Unit]]("partial.recovers")
      .getOrElse {
    val buffer = new ListBuffer[() => Unit]
    ctx.update("partial.recovers", buffer)
    buffer
  }

  def apply(actions: => Unit): Nothing = {
    if (!request.isXHR) sendError(404)
    try {
      actions
    } catch {
      case e: ValidationException =>
        notices.addErrors(e.errors)
        recovers.foreach(_.apply())
      case e: ResponseSentMarker =>
        recovers.foreach(_.apply())
    }
    sendJson("{" + toJsonString + "}")
  }

  def addRecover(func: () => Unit) {
    recovers += func
  }

  def toJsonString = {
    val sb = new StringBuilder
    sb.append(notices.toJsonString)
    val params = paramsMap.toSeq ++
        ctx.getString("redirect").map(s => "redirect" -> s).toSeq
    if (params.size > 0) {
      sb.append(",")
      sb.append(web.toJsonString(params: _*))
    }

    if (JSONParams.size > 0) {
      JSONParams.foreach { case (key, value) =>
        sb.append(",")
        sb.append("\"" + key + "\":" + value)
      }
    }
    sb.toString
  }

}

object ApiV1 {
  def checkParams() {
    if (auth.isEmpty)
      jsonError("api.v1.user.notAuthenticated")

    if (!session.contains("api:key"))
      jsonError("api.v1.user.notAuthenticated")

    if (!request.params.contains("principal"))
      jsonError("api.v1.user.notAuthenticated")

    val u = User.fetchOption(request.params("principal"))
        .getOrElse(jsonError("api.v1.user.notProvided"))

    if (u != auth.principal)
      jsonError("api.v1.user.missChecked")


    // We don't want to get the real ApiKey from client.
    val nonce = request.params.getString("nonce")
        .getOrElse(jsonError("api.v1.nonce.notProvided"))

    if (request.params.get("apiKey").getOrElse("") != sha256(nonce + ":" + session.getString("api:key").get))
      jsonError("api.v1.key.mismatch")

  }

  def jsonError(msg: String) = {
    sendJson(
      """
        { "error": "%s" }
      """.stripMargin.format(new Msg(msg).toString())
    )
  }
}

class ApiV1Router extends Router {

  post("/login") = apiPartial {
    User.lookup(param("e")).map { u =>
    // Client must generate nonce and then send it to the server with encoded password: sha256(nonce:pswdSha256)
    // We will never get the password and even sha256 of the password.
      val nonce = param("n")
      val p = param("p")
      if (sha256(nonce + ":" + u.pswdSha256()) == p) {
        // No "remember me" cookies for API requests (at least for now).
        auth.login(u, false)
        val apiKey = randomUUID
        session.update("api:key", apiKey)
        apiPartial.addParam("apiKey", apiKey)
        apiPartial.addJSONParam("user", u.toJSONObject)
      }
    }.getOrElse {
      apiPartial.addParam("error", "User.notFound")
    }
  }

  // After login user gets his temporary key and must send it with it every request.
  ApiV1.checkParams()

  val user = auth.principal

  get("/user") = apiPartial {
    apiPartial.addJSONParam("user", user.toJSONObject)
  }

  get("/todo/list.json") = {
    val todos = Todo.find(user, param("sorting"))
    sendJson(json.toString(todos))
  }

  get("/todo/:id.json") = {
    val todo = Todo.fetch(param("id"))
    if (todo.user() != user)
      sendError(404)

    sendJson(todo.toJSONObject)
  }

  put("/todo") = apiPartial {
    val todo = new Todo
    todo.updatePrimitiveFieldsFromParams()
    todo.user := user
    todo.save()
    notices.addInfo("todo.added")
    apiPartial.addJSONParam("todo", todo.toJSONObject)
  }

  post("/todo/:id.json") = apiPartial {
    val todo = Todo.fetch(param("id"))
    if (todo.user() != user)
      sendError(404)

    todo.updatePrimitiveFieldsFromParams()
    todo.save()
    notices.addInfo("saved")
    apiPartial.addJSONParam("todo", todo.toJSONObject)
  }

  delete("/todo/:id") = apiPartial {
    val todo = Todo.fetch(param("id"))
    if (todo.user() != user)
      sendError(404)

    todo.DELETE_!()
    notices.addInfo("todo.deleted")
  }

}

