[#ftl]

[#assign content]
<div id="container-480">
  <form class="pad partial"
        method="post"
        action="/${todo.id}">
    <h2>${msg['todo.edit.title']}</h2>
    [#include "edit-form.p.ftl"/]
    <div class="submits centered margin-top">
      <input type="submit" value="${msg['Save']}" class="btn">
      <span>${msg['or']}</span>
      <a href="/" class="partial-link">${msg['cancel']}</a>
    </div>
  </form>
</div>
[/#assign]

[#include "layout.ftl"/]