package org.todo

import circumflex._, core._, web._, freemarker._

class TodoRouter extends Router {

  auth.require()

  get("/?") = {
    val sorting = param("s")
    val completed = param("c") == "true"
    'todos := Todo.find(currentUser, sorting, completed)
    ftl("/todo/list.ftl")
  }

  get("/~new") = ftl("/todo/new.ftl")

  post("/?") = partial {
    val todo = new Todo
    todo.user := currentUser
    todo.updateFromParams()
    todo.save()
    notices.addInfo("todo.added")
    'redirect := prefix + "/"
  }

  sub("/:id") = {
    val todo = Todo.fetch(param("id"))
    'todo := todo

    // You can't see todo of another user
    if (todo.user() != currentUser)
      sendError(404)

    get("/?") = ftl("/todo/edit.ftl")

    post("/?") = partial {
      todo.updateFromParams()
      todo.save()
      notices.addInfo("saved")
    }

    get("/~delete") = ftl("/todo/delete.ftl")

    delete("/?") = partial {
      todo.DELETE_!()
      notices.addInfo("todo.deleted")
      'redirect := "/"
    }
  }

}