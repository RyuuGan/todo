[#ftl]
[#if (request.headers['X-Render-Mode']!"") == "partial-viewport"]

<div id="partial-viewport">
${content}
</div>

[#else]
  [#assign content]
  <div id="partial-viewport">
  ${content}
  </div>
  <script type="text/javascript">
    $.scalpel.settings['viewport.container'] = "#partial-viewport"
  </script>
  [/#assign]
  [#include "../layout.ftl"/]

[/#if]