[#ftl]

[#assign content]
<div id="container-480">
  <form class="pad partial"
        method="post"
        action="/">
    <h2>${msg['todo.new.title']}</h2>
    [#include "edit-form.p.ftl"/]
    <div class="submits centered margin-top">
      <input type="submit" value="${msg['todo.new']}" class="btn">
      <span>${msg['or']}</span>
      <a href="/" class="partial-link">${msg['cancel']}</a>
    </div>
  </form>
</div>
[/#assign]

[#include "layout.ftl"/]