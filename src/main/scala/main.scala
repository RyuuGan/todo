package org.todo

import circumflex._, core._, web._, freemarker._, security._

class Main extends Router {

  new SecurityRouter(auth)

  sub("/auth") = new AuthRouter

  sub("/api/v1") = new ApiV1Router

  new TodoRouter
}