[#ftl]

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
  <head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <link rel="stylesheet/less"
          type="text/css"
          media="screen"
          href="/less/main.less"/>
    <script type="text/javascript" src="/js/less.min.js">
    </script>
    <script type="text/javascript" src="http://code.jquery.com/jquery-2.0.2.min.js">
    </script>
    <script type="text/javascript" src="http://scalpeljs.github.io/scalpeljs-0.1.js">
    </script>
    <script type="text/javascript" src="/js/app.js">
    </script>
    <script type="text/javascript" src="/js/api.js">
    </script>
    <title>${msg['title']}</title>
  </head>
  <body data-viewport-prefix="/">
    <div id="notices"></div>
    <div id="header">
      <a href="/" class="partial-link">${msg['title']}</a>
    [#if !auth.isEmpty]
      <div class="user-nav">
        <span class="elem">${msg['loggedIn.as']} ${auth.principal.title}</span>
        <a href="/auth/logout">
          <img title="${msg['auth.logout']}"
               src="/img/icons/32/lock.png"/>
          <span>${msg['auth.logout']}</span>
        </a>
      </div>
    [/#if]
    </div>
    <div id="outer">
      <div id="content">
        <div id="viewport">
        ${content}
        </div>
      </div>
      <div id="footer">
        <a class="home" href="http://${request.headers['Host']!"localhost"}">
        ${request.headers['Host']!"localhost"}
        </a>
        <div>Icons by <a href="http://dryicons.com">DryIcons</a></div>
      </div>
    </div>
    <script type="text/javascript">
      $.scalpel.queue['body'] = function() {
        if (typeof(history.replaceState) == "function")
          window.history.replaceState({href: window.location.href}, "", window.location.href);
      }
    </script>
  </body>
</html>