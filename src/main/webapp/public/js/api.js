var apiV1 = {
  initialized: false,
  user: function(){
    var jsonUser = window.sessionStorage.getItem("api:v1:user");
    if (jsonUser)
      try {
        var user = JSON.parse(jsonUser);
        if (apiV1.initialized)
          return user;
        var u;
        $.ajax({
          type: "get",
          async: false,
          data: {
            "principal": user.id,
            "nonce": nonce,
            "apiKey": $.sha256(nonce + ":" + apiV1.apiKey()),
            "__": new Date().toString()
          },
          dataType: "json",
          url: "/api/v1/user",
          success: function(data){
            u = data.user;
          }
        }, "json");
        if (u != undefined && u.id == user.id) {
          apiV1.initialized = true;
          return user
        }
      } catch (e) {
      }
    return undefined;
  },
  nonce: function() {
    return Math.random().toString(36).substring(5);
  },
  apiKey: function(){
    return window.sessionStorage.getItem("api:v1:apiKey")
  },
  login: function(email, password) {
    var nonce = apiV1.nonce();
    $.ajax({
      type: "post",
      async: false,
      data: { "e": email,
        "n":nonce,
        "p": $.sha256(nonce + ":" + $.sha256(password))
      },
      dataType: "json",
      url: "/api/v1/login",
      success: function(data){
        window.sessionStorage.setItem("api:v1:user", JSON.stringify(data.user));
        window.sessionStorage.setItem("api:v1:apiKey", data.apiKey);
        apiV1.initialized = true;
        $.scalpel.ajax.processResponse(data);
      }
    });
  },

  todo: {
    list: function(sorting) {
      if (!apiV1.user() || !apiV1.apiKey())
        return;
      var nonce = apiV1.nonce();
      $.get("/api/v1/todo/list.json", {
        "principal": apiV1.user().id,
        "nonce": nonce,
        "apiKey": $.sha256(nonce + ":" + apiV1.apiKey()),
        "sorting": sorting
      }, function(data, status, xhr){
        console.log(data)
      }, "json")
    },

    /* Note that todo must have a description field
    *  other fields are optional. You can specify any of the next fields:
    *  priority, due_start_date, due_end_date, completion_date. All date must have `yyyy-MM-dd` format.
    *  */

    add: function(todo) {
      if (!apiV1.user() || !apiV1.apiKey())
        return;
      var nonce = apiV1.nonce();
      var data = {
        "principal": apiV1.user().id,
        "nonce": nonce,
        "apiKey": $.sha256(nonce + ":" + apiV1.apiKey()),
      };
      $.extend(data, todo);
      $.ajax({
        type: "put",
        data: data,
        dataType: "json",
        url: "/api/v1/todo",
        success: function(data, status, xhr){
          $.scalpel.ajax.processResponse(data);
          console.log(data)
        }
      });
    },

    delete: function(id) {
      if (!apiV1.user() || !apiV1.apiKey())
        return;
      var nonce = apiV1.nonce();
      $.ajax({
        type: "post",
        data: {
          "principal": apiV1.user().id,
          "nonce": nonce,
          "apiKey": $.sha256(nonce + ":" + apiV1.apiKey()),
          "_method": "delete"
        },
        dataType: "json",
        url: "/api/v1/todo/" + id,
        success: function(data, status, xhr){
          $.scalpel.ajax.processResponse(data);
          console.log(data)
        }
      });
    },

    // This is method is like the add, but todo MUST exists on the server.

    edit: function(todo) {
      if (!todo.id) {
        console.log("No id specified for todo to update.");
      }
      if (!apiV1.user() || !apiV1.apiKey())
        return;
      var nonce = apiV1.nonce();
      var data = {
        "principal": apiV1.user().id,
        "nonce": nonce,
        "apiKey": $.sha256(nonce + ":" + apiV1.apiKey())
      };
      $.extend(data, todo);
      $.ajax({
        type: "post",
        data: data,
        dataType: "json",
        url: "/api/v1/todo/" + todo.id + ".json",
        success: function(data, status, xhr){
          $.scalpel.ajax.processResponse(data);
          console.log(data)
        }
      });
    }
  }

};