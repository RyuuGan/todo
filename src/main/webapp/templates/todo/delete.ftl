[#ftl]

[#assign content]
<div id="container-480">
  <form class="pad partial"
        method="post"
        action="/${todo.id}">
    <input type="hidden" name="_method" value="delete"/>
    [#include "/locale/todo.delete.ftl"/]
    <div class="submits centered margin-top">
      <input type="submit" value="${msg['delete.confirm']}" class="btn">
      <span>${msg['or']}</span>
      <a href="/" class="partial-link">${msg['cancel']}</a>
    </div>
  </form>
</div>
[/#assign]

[#include "layout.ftl"/]